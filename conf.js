//arquivo conf.js

exports.config = {
seleniumAddress:'http://localhost:4444/wd/hub',//onde o selenium está rodando
//specs: ['../tests/specs/*.js'],
framework: 'jasmine',
specs:['./tests/specs/spec.js'],
baseUrl:"http://www.way2automation.com",
//allScriptsTimeout: 30000
jasmineNodeOpts: {
    showcolors: true,
    silent: true,
    defaultTimeoutInterval: 360000,
    print: function(){
    }

},
onPrepare:()=>{
    var AllureReporter = require('Jasmine-allure-reporter');
    jasmine.getEnv().addReporter(new AllureReporter({
        resultsDir: 'allure-results'

    }));

    var SpecReporter = require('Jasmine-spec-reporter').SpecReporter;
    jasmine.getEnv().addReporter(new SpecReporter({
        spec:{
            displayStacktrace: true, 
            displayErrorMessages: true,
            displayFailed: true,
            displayDuration: true
    },
        summary:{
            displayErrorMessages: true,
            displayStacktrace: false,
            displaySuccessfull: true,
            displayFailed: true,
            displayDuration: true
    },
        colors:{
            enabled: true
    }

}));
},
/*,capabilities:{
    //alterando o navegador default para execução dos testes
    browserName:"Firefox"
}
//configurando para rodar os teste em mais de um browser
, multicapabilities:[{
    browserName:'chrome'},
{ 
    browserName:'Firefox'}]*/
}