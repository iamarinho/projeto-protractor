/**
* @file helper.js
*/
var Helper = this;
    Helper.takeshot = (function (){
        browser.sleep(1000);
        browser.takeScreenshot().then((png) => {
            allure.createAttachment('Screenshot', () =>{
                return new Buffer(png, 'base64')
            },'image/png')();
            return this;
        })
    });

module.exports = Helper;

