// arquivo spec.js

var LoginPage = require('../pages/loginpage.po.js');
var Helper = require('../helper');
var originalTimeout;

browser.ignoreSynchronization = false;

beforeEach(function () {
  originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
  jasmine.DEFAULT_TIMEOUT_INTERVAL = 100000;
  LoginPage.get();
});

afterEach(() =>{
  jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
   Helper.takeshot();
 });

describe('Login Feature', function () {
  it('Login successfull', function () {
    LoginPage.login('angular', 'password', 'Teste');
    browser.driver.sleep(11000);
    expect(LoginPage.successMessage.isDisplayed()).toBe(true);
  });

  it('Incorrect User', function () {
    LoginPage.login('incorrect', 'password', 'Teste');
    expect(LoginPage.incorrectCredentials.isDisplayed()).toBe(true);
  });

  it('Incorrect Password', function () {
    LoginPage.login('angular', '5555', 'Teste');
    expect(LoginPage.incorrectCredentials.isDisplayed()).toBe(true);
  });
  it('User field not filled', function () {
    LoginPage.fillForm('', 'password', 'Teste');
    expect(LoginPage.loginButton.isEnabled()).toBe(false);
  });
  it('Password field not filled', function () {
    LoginPage.fillForm('angular', '', 'Teste');
    expect(LoginPage.loginButton.isEnabled()).toBe(false);
  });
  it('Logout successfull', function () {
    LoginPage.login('angular', 'password', 'Teste');
    LoginPage.logout();
    expect(LoginPage.user.isPresent()).toBe(true);
  });

});